$param = $args[0]
$time = $args[1]

if ($param -eq 'shutdown') {
   Write-Host 'Your computer will shutdown in'$time 'second(s)'
   if ($time) {
        Start-Sleep -Seconds $time
        Stop-Computer
   }     
}
elseif ($param -eq 'lock') {
   Write-Host 'Your computer will be lock in'$time 'second(s)' 
   if ($time) {
        Start-Sleep -Seconds $time
        rundll32.exe user32.dll,LockWorkStation        
   }
}